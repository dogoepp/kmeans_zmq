
#include "matrice.h"


void deleteLine(matrice * A,int line){
    matrice r = zeros(A->n-1,A->m);
    int d = 0;
    for (int i = 0; i < A->n; ++i)
    {
        if(i != line) {
            for (int j = 0; j < A->m; ++j)
            {
                r.contenu[d][j] = A->contenu[i][j];
            }
            d++;
        }
    }
    // matrice * del = A;
    freeMatrice(A);
    copy(A,r);

    freeMatrice(&r);
}


matrice comatrice(matrice A){
    matrice r;
    r.n=(A.n);
    r.m=(A.m);
    int i,j;
    r.contenu = (long double **) malloc((A.n)*sizeof(long double*));
    for (i=0; i<A.n ; i++)
         r.contenu[i] = (long double *)malloc((A.m)* sizeof(long double));

    for(i=0;i<A.n;i++){
        for(j=0;j<A.m;j++){
            r.contenu[i][j]=(pow(-1,(i+j)))*determinant(copySauf(A,i,j));
        }
    }


    return r;
}
matrice copySauf(matrice A,int ligne, int colonne){
    matrice r;
    r.n=(A.n -1);
    r.m=(A.m -1);
    int i,j;
    r.contenu = (long double **) malloc((A.n -1)*sizeof(long double*));
    for (i=0; i<A.n - 1; i++)
         r.contenu[i] = (long double *)malloc((A.m - 1)* sizeof(long double));
    int c,d;
    c=0;
    for( i=0;i<A.n;i++){
        d=0;
          if(i!=ligne){
            for( j=0;j<A.m;j++){
                if(j!=colonne){
                    r.contenu[c][d]=A.contenu[i][j];
                    d=d+1;
                }
           }
           c=c+1;
         }
    }

    return r;
}
matrice id(int n){
    matrice r=zeros(n,n);
    int i;
    for( i=0;i<n;i++){
        r.contenu[i][i]=1;
    }
    return r;
}
matrice product(matrice A, matrice B){
    matrice r;
    r.n=(A.n);
    r.m=(B.m);
    int i,j,k;
    r.contenu = (long double **) malloc(A.n*sizeof(long double*));
    for (i=0; i<A.n; i++)
         r.contenu[i] = (long double *)malloc(B.m* sizeof(long double));
    long double a=0;
    for(i=0;i<A.n;i++){
        for(j=0;j<B.m;j++){
            for( k=0;k<B.n;k++){
                a=a+A.contenu[i][k]*B.contenu[k][j];
            }
            r.contenu[i][j]=a;
            a=0;
        }
    }
    return r;


}
void copy(matrice * copi, matrice A){
    
    copi->n = A.n;
    copi->m = A.m;
    copi->contenu = (long double **) malloc(copi->n*sizeof(long double *));
    for (int i=0; i<A.n; i++){
        copi->contenu[i] = (long double *)malloc(copi->m* sizeof(long double));
        for(int j=0;j<A.m;j++){
            copi->contenu[i][j] = A.contenu[i][j];
        }
    }
}
matrice sum(matrice A, matrice B){
    matrice r;
    r.n=(A.n);
    r.m=(A.m);
    int i,j;
    r.contenu = (long double **) malloc(A.n*sizeof(long double*));
    for (i=0; i<A.n; i++)
         r.contenu[i] = (long double *)malloc(A.m* sizeof(long double));

    for(i=0;i<A.n;i++){
        for(j=0;j<A.m;j++){
            r.contenu[i][j]=A.contenu[i][j]+B.contenu[i][j];
        }
    }

    return r;

}

matrice sous(matrice A, matrice B){
    matrice r;
    r.n=(A.n);
    r.m=(A.m);
    int i,j;
    r.contenu = (long double **) malloc(A.n*sizeof(long double*));
    for (i=0; i<A.n; i++)
         r.contenu[i] = (long double *)malloc(A.m* sizeof(long double));

    for(i=0;i<A.n;i++){
        for(j=0;j<A.m;j++){
            r.contenu[i][j]=A.contenu[i][j]-B.contenu[i][j];
        }
    }

    return r;
}
// soustrait la deuxième matrice de la première, resultat dans la premiere
void sous_interne(matrice *A, matrice B){

    int i,j;
    for(i=0;i<A->n;i++){
        for(j=0;j<A->m;j++){
            A->contenu[i][j]-= B.contenu[i][j];
        }
    }

}

void som_interne(matrice *A, matrice B){

    int i,j;
    for(i=0;i<A->n;i++){
        for(j=0;j<A->m;j++){
            A->contenu[i][j]+= B.contenu[i][j];
        }
    }

}
matrice sousMat(matrice A,int ligne){
    matrice r;
    r.n=(A.n-1);
    r.m=(A.m-1);
    int i,j;
    r.contenu = (long double **) malloc((A.n-1)*sizeof(long double*));
    for (i=0; i<(A.n-1); i++)
         r.contenu[i] = (long double *)malloc((A.m-1) * sizeof(long double));
    int c,d;
    c=0;
    d=0;

    for( i=0;i<A.n;i++){
          if(i!=ligne){
            for( j=1;j<A.m;j++){
                r.contenu[c][d]=A.contenu[i][j];
                d=d+1;
           }
           c=c+1;
         }

          d=0;
    }
    return r;


}
double determinant(matrice A){
    if(A.n==1 && A.m==1) return A.contenu[0][0];
    double d=0;
    int i;
    for ( i=0;i<A.n;i++){
        d=d+ ((pow(-1,i)) * A.contenu[i][0] * determinant(sousMat(A,i)));
    }

    return d;
}
matrice transpose(matrice A){
    matrice r;
    r.n=(A.m);
    r.m=(A.n);
    int i,j;
    r.contenu = (long double **) malloc(A.m*sizeof(long double*));
    for (i=0; i<A.m; i++)
         r.contenu[i] = (long double *)malloc(A.n* sizeof(long double));

    for(i=0;i<A.n;i++){
        for(j=0;j<A.m;j++){
            r.contenu[j][i]=A.contenu[i][j];
        }
    }

    return r;

}

void extractLine(matrice * line, matrice A,int l){
    line->n = 1;
    line->m = A.m;
    line->contenu = (long double **) malloc(sizeof(long double *));
    line->contenu[0] = (long double *)malloc(line->m* sizeof(long double));
    for(int j=0;j<A.m;j++){
        line->contenu[0][j] = A.contenu[l][j];
    }
}
void extractColumn(matrice * column, matrice A, int c){
    column->n = A.n;
    column->m = 1;
    column->contenu = (long double **) malloc(column->n * sizeof(long double *));
    for (int i = 0; i< column->n; i++){
        column->contenu[i] = (long double *)malloc(sizeof(long double));
        column->contenu[i][0] = A.contenu[i][c];
    }
}

void extract(matrice * r, matrice A,int n1, int n2, int m1, int m2){
    /*extraire la sous matrice de la ligne n1 à n2 et de la colonne m1 à m2 (inclus)*/
    r->n=(n2-n1+1);
    r->m=(m2-m1+1);
    int i;
    r->contenu = (long double **) malloc((n2-n1+1)*sizeof(long double*));
    for (i=0; i<(n2-n1+1); i++)
         r->contenu[i] = (long double *)malloc((m2-m1+1) * sizeof(long double));
    int j,c,d;
    c=0;
    d=0;
    for(i=n1;i<=n2;i++){
        for(j=m1;j<=m2;j++){
            r->contenu[c][d]=A.contenu[i][j];
            d=d+1;
        }
        d=0;
        c=c+1;
    }

}
void concatLine(matrice * initiale, matrice A){
  if(initiale->m == A.m){
    int t = initiale->n;
    initiale->n += A.n;
   initiale->contenu = (long double **)realloc(initiale->contenu,initiale->n * sizeof(long double *));
   for(int i = 0;i<A.n;i++){
        initiale->contenu[t+i] = (long double *)malloc(initiale->m*sizeof(long double));
        for(int j = 0;j<initiale->m;j++)
            initiale->contenu[t+i][j] = A.contenu[i][j];
   }
    
  }
}

void concatColoumn(matrice * initiale, matrice A){
  if(initiale->n == A.n){
   int t = initiale->m;
   initiale->m += A.m;

   for(int i = 0; i < initiale->n; i++){
    initiale->contenu[i] = (long double *)realloc(initiale->contenu[i],(initiale->m) * sizeof(long double));
    for(int j=0;j<A.m;j++){
        initiale->contenu[i][t+j] = A.contenu[i][j];
      }
   }
  }
}
matrice init_value(int n,int m,long double value){
    matrice ma;
    ma.n=n;
    ma.m=m;
    int i,j;
    ma.contenu = (long double **) malloc(n*sizeof(long double*));
    for (i=0; i<n; i++)
         ma.contenu[i] = (long double *)malloc(m * sizeof(long double));


    for ( i=0; i<n; i++)
    {
        for(j=0;j<m;j++){
            ma.contenu[i][j]=value;
        }
    }
    return ma;
}

matrice ones(int n, int m){
    matrice ma;
    ma.n=n;
    ma.m=m;
    int i,j;
    ma.contenu = (long double **) malloc(n*sizeof(long double*));
    for (i=0; i<n; i++)
         ma.contenu[i] = (long double *)malloc(m * sizeof(long double));


    for ( i=0; i<n; i++)
    {
        for(j=0;j<m;j++){
            ma.contenu[i][j]=1;
        }
    }
    return ma;
}
matrice random_matrice(int n,int m){
    srand(time(0));
    matrice ma;
    ma.n=n;
    ma.m=m;
    int i,j;
    ma.contenu = (long double **) malloc(n*sizeof(long double*));
    for (i=0; i<n; i++)
         ma.contenu[i] = (long double *)malloc(m * sizeof(long double));


    for ( i=0; i<n; i++)
    {
        for(j=0;j<m;j++){
            ma.contenu[i][j]=random_(1,10);
        }
    }
    return ma;
}
matrice two(int n, int m){
    matrice ma;
    ma.n=n;
    ma.m=m;
    int i,j;
    ma.contenu = (long double **) malloc(n*sizeof(long double*));
    for (i=0; i<n; i++)
         ma.contenu[i] = (long double *)malloc(m * sizeof(long double));


    for ( i=0; i<n; i++)
    {
        for(j=0;j<m;j++){
            ma.contenu[i][j]=2;
        }
    }
    return ma;
}

matrice zeros(int n,int m){
    matrice ma;
    ma.n=n;
    ma.m=m;
    int i,j;
    ma.contenu = (long double **) malloc(n*sizeof(long double*));
    for (i=0; i<n; i++)
         ma.contenu[i] = (long double *)malloc(m * sizeof(long double));

    for ( i=0; i<n; i++)
    {
        for(j=0;j<m;j++){
            ma.contenu[i][j]=0;
        }
    }
    return ma;
}

matrice inverse(matrice M){

    matrice aux;
    copy(&aux,M);
    int i,k,c,l;
    float _max;
    matrice diag=zeros(M.n,M.m);
    for(i=0;i<M.n;i++){
        diag.contenu[i][i]=1;
    }
    concatColoumn(&aux,diag);
    for (int k = 0; k < aux.n; k++)
    {
        _max = aux.contenu[k][k];
        l = k;
        for (int i = k+1; i < aux.n; i++)
        {
           if (fabs(aux.contenu[i][k])>_max){
            _max =fabs(aux.contenu[i][k]);
            l = i;
           }
        }
        if(_max ==0){ 
            //printf("ligne %d \n",k);
            //print(aux);
            printf("Impossible de calculer l'inverse de la matrice\n\n");
            exit(-1);
       }

        if(k != l){
            matrice auxi;
            extractLine(&auxi,aux,k); // on copie toute la ligne k dans l
            for(c=0;c<aux.m;c++){

                aux.contenu[k][c]=aux.contenu[l][c];
                aux.contenu[l][c]=auxi.contenu[0][c];
            }
            freeMatrice(&auxi);
        }

        if(aux.contenu[k][k]!=1) {//diviser_pivot(M,k);

            long double pivot =aux.contenu[k][k];
            for (c=k;c<aux.m;c++){
                aux.contenu[k][c] /= pivot;
            } 

        }
    

        for(i=0;i<aux.n;i++){
            if(i!=k){
                long double aik = aux.contenu[i][k];
                for(c=k;c<aux.m;c++){
                   aux.contenu[i][c]-= aik * aux.contenu[k][c];

                }

            }
        }
    
    }

    matrice r;
    extract(&r,aux,0,aux.n-1,aux.m/2,aux.m-1);
    freeMatrice(&aux);

    return r;
}

void print(matrice A){
    int n = A.n;
    int m = A.m;
    int i,j;

    printf("\n\n");
    for( i=0;i<n;i++){
        printf("%d |",(i+1));
        for( j=0;j<m;j++){
            if(j!=m-1) printf("%Lf\t",A.contenu[i][j]);
            else printf("%Lf",A.contenu[i][j]);
        }
        printf("|\n");
        }

}
int compter_nb_ligne(char * filename){
    int TAILLE = 500;
    FILE * fichier = fopen(filename, "r");
    char ligne [TAILLE];
    int nLignes = 0;

    while(fgets(ligne,TAILLE,fichier)!=NULL)
    {
        nLignes++;
    }
    fclose(fichier);
    return nLignes;
} 



void init(matriceChar *A,int n ,int m){
    A->n = n;
    A->m = m;
    A->contenu = (char ***)malloc(n*sizeof(char **));
    for(int i = 0 ; i < n ; i++){
        A->contenu[i] = (char **)malloc(m*sizeof(char*));
        for(int j = 0; j< m; j++){
            A->contenu[i][j] = (char *)malloc(1*sizeof(char*));
        }
    }

}
int countword(matriceChar A,char* mot,int colone){
    int  c = 0;
    for(int i = 0; i < A.n; i++){
            if(strcmp(A.contenu[i][colone],mot)==0) c++;
    }
    return c;
}
int countwordWhere(matriceChar A,char* mot,int colone,matriceChar B,char* classe){
    int  c = 0;
    for(int i = 0; i < A.n; i++){
            if(strcmp(A.contenu[i][colone],mot)==0 && (strcmp(B.contenu[i][0],classe)==0)) c++;
    }
    return c;
}
matriceChar concatLineC(matriceChar initiale,matriceChar A){
    if(initiale.m == A.m){
    matriceChar r ;
    init(&r,initiale.n+A.n,A.m);

    int i,j;
    for( i=0;i<initiale.n;i++){
      for( j=0;j<initiale.m;j++){
        strcpy(r.contenu[i][j],initiale.contenu[i][j]);
        //r.contenu[i][j]=initiale.contenu[i][j];
      }
    }
    for( i=0;i<A.n;i++){
      for( j=0;j<A.m;j++){
        strcpy(r.contenu[initiale.n +i][j],A.contenu[i][j]);
        //r.contenu[initiale.n +i][j]=A.contenu[i][j];
      }
    }
    return r;
  }
  else{
    return vide((initiale.n+A.n),initiale.m);
  }
}
matriceChar vide(int n,int m){
    matriceChar r;
    init(&r,n,m);
    int i,j;
     for ( i=0; i<n; i++)
    {
        for(j=0;j<m;j++){
            strcpy(r.contenu[i][j],"");
            //ma.contenu[i][j]=0;
        }
    }
    return r;
}
int containt(matriceChar A,char* mot,int colone){
    for(int i = 0; i < A.n; i++){
            if(strcmp(A.contenu[i][colone],mot)==0) return 1;
    }
    return 0;
}
int containtM(matrice A,long double  mot,int colone){
    for(int i = 0; i < A.n; i++){
            if(A.contenu[i][colone]==mot)return 1;
    }
    return 0;
}
matriceChar selectDistinct(matriceChar A,int colone){
    matriceChar r,aux;
    for(int i = 0; i < A.n; i++){
            aux = vide(1,1);
            strcpy(aux.contenu[0][0],A.contenu[i][colone]);
            if (i == 0) r = aux;
            else if (containt(r,aux.contenu[0][0],0)==1) continue;
            else r = concatLineC(r,aux);
    } 

    return r;
}
matrice selectDistinctM(matrice A,int colone){
    matrice r,aux;
    for(int i = 0; i < A.n; i++){
            aux = zeros(1,1);
            aux.contenu[0][0]=A.contenu[i][colone];
            if (i == 0) r = aux;
            else if (containtM(r,aux.contenu[0][0],0)==1) continue;
            else concatLine(&r,aux);
    } 

    return r;
}
int random_(int min, int max){
     
   return rand() %(max-min+1) + min ; 
}


void mixer_matrice(matrice * A){
srand(time(0));
    if(A->n ==1) return;
    matrice r;
    extractLine(&r,*A,0);
    deleteLine(A,0);

    int rando;
    while (A->n != 1){
        rando = random_(0,A->n-1);
        matrice aux;
        extractLine(&aux,*A,rando);
        concatLine(&r,aux);
        freeMatrice(&aux);
        deleteLine(A,rando);
    }
    concatLine(&r,*A);
    copy(A,r);
    freeMatrice(&r);


}

void mixer_matrices(matrice * A,matrice * B){
    srand(time(0));
    if(A->n ==1) return;
    matrice r;
    matrice s;
    extractLine(&r,*A,0);
    deleteLine(A,0);

    extractLine(&s,*B,0);
    deleteLine(B,0);
    int rando;
    while (A->n != 1){
        rando = random_(0,A->n-1);
        matrice aux;
        matrice aux2;
        extractLine(&aux,*A,rando);
        concatLine(&r,aux);
        freeMatrice(&aux);
        deleteLine(A,rando);

        extractLine(&aux2,*B,rando);
        concatLine(&s,aux2);
        freeMatrice(&aux2);
        deleteLine(B,rando);
    }
    concatLine(&r,*A);
    copy(A,r);
    freeMatrice(&r);

    concatLine(&s,*B);
    copy(B,s);
    freeMatrice(&s);


}

matriceChar extractLineC(matriceChar A,int n){
    matriceChar  r;
    init(&r,1,A.m);
    for (int j = 0; j< A.m; j++){
        strcpy(r.contenu[0][j],A.contenu[n][j]);
    }
    return r;

}
matriceChar extractC(matriceChar A,int n1, int n2, int m1, int m2){
    /*extraire la sous matrice de la ligne n1 à n2 et de la colonne m1 à m2*/
    matriceChar r;
    r = vide(n2-n1+1,m2-m1+1);
    int i,j,c,d;

    c=0;
    d=0;
    for(i=n1;i<=n2;i++){
        for(j=m1;j<=m2;j++){
            r.contenu[c][d]=A.contenu[i][j];
            d=d+1;
        }
        d=0;
        c=c+1;
    }

    return r;
}
void printC(matriceChar A){
    int n = A.n;
    int m = A.m;
    int i,j;

    printf("\n\n");
    for( i=0;i<n;i++){
        printf("%d |",(i+1));
        for( j=0;j<m;j++){
            if(j!=m-1) printf("%s\t",A.contenu[i][j]);
            else printf("%s",A.contenu[i][j]);
        }
        printf("|\n");
        }

}
void freeMatrice(matrice * A){
    for(int i = 0;i < A->n; i++){
        free(A->contenu[i]);
    }
    free(A->contenu);
    A->n = 0;
    A->m = 0;
}

void readMatrice(matrice * A, char * filename){
    int TAILLE = 500;
     FILE *f = fopen(filename,"r");
    char ligne[TAILLE];
    char delimit[]=" \t,;";
    char * mot;
    int df,c = 0;
    while(fgets(ligne, TAILLE, f) != NULL){
        df = 0;
        mot = strtok(ligne, delimit);
        matrice line = ones (1,1);
        line.contenu[0][0] = atof(mot);
        while(mot = strtok(NULL, delimit)){
            df++;
            matrice aux = ones (1,1);
            aux.contenu[0][0] = atof(mot);
            concatColoumn(&line,aux);
            freeMatrice(&aux);
        }
        if (c == 0) {copy(A,line);}
        else {concatLine(A, line);}
        freeMatrice(&line);  
        c++;
       
    }
    fclose(f);
}

void readDatas(matrice * A,matrice * B, char * filename ,int ncolumn){
    int TAILLE = 500;
     FILE *f = fopen(filename,"r");
    char ligne[TAILLE];
    char delimit[]=" \t,;";
    char * mot;
    int df,c = 0;
    while(fgets(ligne, TAILLE, f) != NULL){
        df = 0;
        mot = strtok(ligne, delimit);
        matrice line = ones (1,1);
        matrice lab = ones (1,1);
        line.contenu[0][0] = atof(mot);
        while(mot = strtok(NULL, delimit)){
            df++;
            matrice aux = ones (1,1);
            aux.contenu[0][0] = atof(mot);
            if(df == ncolumn-1) lab.contenu[0][0] = aux.contenu[0][0];
            else concatColoumn(&line,aux);
            freeMatrice(&aux);
        }
        if (c == 0) {copy(A,line);copy(B,lab);}
        else {concatLine(A, line);concatLine(B, lab);}
        freeMatrice(&line);
        freeMatrice(&lab);  
        c++;
       
    }
    fclose(f);
}
int contient(matrice A,long double v){
    for (int i = 0; i < A.n; i++)
    {
        for (int j = 0; j < A.m; j++)
        {
            if(A.contenu[i][j]==v) return 1;
        }
    }
    return 0;
}
void produit_scalaire_matrice(long double alpha,matrice *r){
    for (int i = 0; i < r->n; i++)
    {
        for (int j = 0; j < r->m; j++)
        {
            r->contenu[i][j] *=alpha;
        }
    }
}

void write_in_file(matrice A,char * filename,const char *  sep){
    FILE *fi = fopen(filename,"a");
    for (int i = 0; i < A.n; i++)
    {
       for (int j = 0; j < A.m; j++)
       {
         if(j==A.m-1) fprintf(fi, "%2.10Lf\n",A.contenu[i][j]);
         else fprintf(fi, "%2.10Lf%s",A.contenu[i][j],sep);
       }
    }
        fclose(fi);
}

long double distance(matrice a, matrice b){
  long double f = 0.0;
  for(int i = 0; i<a.m; i++){
    f += pow((a.contenu[0][i] - b.contenu[0][i]), 2); 
  }
  return sqrt(f);
}

matrice moyenne(matrice X){
    matrice moyenne = zeros(1,X.m);
    for (int i = 0; i < X.n; i++)
    {
       for (int j = 0; j < X.m; j++)
       {
         moyenne.contenu[0][j]+=X.contenu[i][j]/X.n;
       }
    }
    return moyenne;

}
matrice normaliser_centrer(matrice X){
    matrice moyen = moyenne(X);
    matrice result = zeros(X.n,X.m);

    for (int i = 0; i < X.n; i++)
    {
       for (int j = 0; j < X.m; j++)
       {
         result.contenu[i][j] = X.contenu[i][j] - moyen.contenu[0][j];
       }
    }
    freeMatrice(&moyen);
    return result;

}