#include "zhelpers.h"
#include "matrice.h"

long double dist(long double a[],long double b[],int n){
  long double f = 0.0;
  for(int i = 0; i<n; i++){
    f += pow((a[i] - b[i]), 2); 
  }
  return sqrt(f);
}

int main (int argc, char const * argv[]) 
{
    //  Socket to receive messages on
    void *context = zmq_ctx_new ();
    void *receiver = zmq_socket (context, ZMQ_PULL);
    zmq_connect (receiver, "tcp://localhost:5557");

    //  Socket to send messages to
    void *sender = zmq_socket (context, ZMQ_PUSH);
    zmq_connect (sender, "tcp://localhost:5558");

    // Socket to receive centroids
    void *subscriber = zmq_socket(context, ZMQ_SUB);
    zmq_connect(subscriber, "tcp://127.0.0.1:5556");
    zmq_setsockopt(subscriber, ZMQ_SUBSCRIBE,"",0);


    zmq_pollitem_t items [] = {
        { receiver,   0, ZMQ_POLLIN, 0 },
        { subscriber, 0, ZMQ_POLLIN, 0 }
    };

    char * k_str = s_recv (subscriber);
    int k = atoi(k_str);

    printf("k == %d\n",k);

    k_str = s_recv (subscriber);
    int m = atoi(k_str);
    printf("m == %d\n",m);

    long double clusters [k][m];
    int size;
    long double centro[m];
    //  Process tasks forever
    long double data[m+1];
    int result[2];
    long double d,min;
    int h = 0;
    char * traitement;
    int c =0;
    int cl = 0;
    int epoch = 0;
    while (1) {
        zmq_poll (items, 2, -1);
        if (items [1].revents & ZMQ_POLLIN) {
            // receive centroids
            //printf("Attent centroide\n");
            epoch++;
            //printf("Reception de clusters (epoch) [%d]\n",epoch/k);

            size = zmq_recv (subscriber, &centro, sizeof(centro), 0);
            if (size != -1) {
                for (int i = 0; i < m; i++)
                {
                    clusters[cl][i] = centro[i];
                }
                cl++;       

            }
        }

        if (items [0].revents & ZMQ_POLLIN) {
            cl = 0;
            //printf("attente donnee\n");
            size = zmq_recv (receiver, &data, sizeof(data), 0);

            if (size != -1) {
                //Reception et traitement des données
                min = dist(data,clusters[0],m);
                c = 0;
                //printf("donnée [%d] => cluster [%d] d = %Lf\n",(int)data[m],0,min);
                for (int i = 1; i < k; i++)
                {
                   d = dist(data,clusters[i],m);
                   if (d < min){
                    c = i;
                    min = d;
                   }
                    //printf("donnée [%d] => cluster [%d] d = %Lf\n",(int)data[m],i,d);


                }

                result[0] = (int)data[m];
                result[1] = c;
                zmq_send (sender, &result, sizeof(result), 0);
            }
        }




    }

    zmq_close (receiver);
    zmq_close (sender);
    zmq_close (subscriber);
    zmq_ctx_destroy (context);
    return 0;
}