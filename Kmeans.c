
#include "zhelpers.h"
#include "matrice.h"
// min et max inclus
int select_random_number(int min, int max){
    srand(time(0));
   return rand() %(max-min+1) + min ; 
}
void initialize_centroid(matrice * list_clusters,matrice X, int k){
    srand(time(0));
    matrice deja = init_value(1,k,-1);
    for(int i = 0; i<k; i++){
        matrice aux;
        int rd,j = 0;
        rd = select_random_number(0,X.n-1);
        while(j < i){
            if(deja.contenu[0][j] != rd) j++;
            else {
                rd = select_random_number(0,X.n-1);
                j = 0;
            }
        }
        deja.contenu[0][i] = rd;
        extractLine(&aux,X,rd);
        copy(&list_clusters[i],aux);
        freeMatrice(&aux);
    }
    freeMatrice(&deja);
}

double calcul_erreur(matrice * old_clusters, matrice * clusters, int k){
    double e = 0.0;
    for(int i =0; i<k; i++){
        e += distance(old_clusters[i],clusters[i]);
    }

    return e/(double)k;
}
int main (int argc, char const * argv[]) 
{
    void *context = zmq_ctx_new ();

    // socket to send centroid to all workers 
    void *publisher = zmq_socket(context, ZMQ_PUB);
    zmq_bind(publisher, "tcp://127.0.0.1:5556");

    //  Socket to send messages on
    void *sender = zmq_socket (context, ZMQ_PUSH);
    zmq_bind (sender, "tcp://*:5557");

    // Socket to receive cluster assignment for each data
    void *receiver = zmq_socket (context, ZMQ_PULL);
    zmq_bind (receiver, "tcp://*:5558");

    // --------------- KMEANS DATA ---------------------
    float seuil = atof(argv[2]);
    int n_epoch = atoi(argv[3]);
    int k = atoi(argv[4]);
    matrice X; 
    int iter = 1;
    double error = seuil;
    readMatrice(&X, argv[1]);
    matrice * clusters = (matrice *)malloc(k * sizeof(matrice));
    matrice * old_clusters;
    initialize_centroid(clusters, X, k);

    for (int i = 0; i < k; ++i)
    {
       print(clusters[i]);
    }




    printf ("Press Enter when the workers are ready: ");
    getchar ();
    printf ("Sending tasks to workers...\n\n\n");


    // Send centroid to all workers
    char rep[256];
    sprintf(rep,"%d",k);
    zmq_send (publisher, rep,255, 0);

    char m_str[256]; // dimension d'un centroid
    sprintf(m_str,"%d",clusters[0].m);
    zmq_send (publisher, m_str,255, 0);


    while(n_epoch >= iter && error >= seuil){

        old_clusters = (matrice *)malloc(k * sizeof(matrice));
        //printf("debut époque..... \n");

        for (int j = 0; j < k; ++j)
        {
            copy(&old_clusters[j],clusters[j]);

        }
       // printf("apres copy\n");

// Envoie des Centroides
        long double centro [clusters[0].m];
        for (int j = 0; j < k; ++j)
        {
            for (int i = 0; i < clusters[0].m; ++i)
            {
                centro[i] = clusters[j].contenu[0][i];
            }
            zmq_send (publisher, &centro, sizeof(centro), 0);

        }

        //printf("Envoie nouveaux centroides\n");

        s_sleep(1);
        // Envoie des données

        long double data [X.m+1];

        for (int i = 0; i < X.n; i++)
        {
            for (int j = 0; j < X.m; j++)
            {
                data [j] = X.contenu[i][j];
            }
            data [X.m] = i;
            zmq_send (sender, &data, sizeof(data), 0);


        }
        int assign[2];
        int nbPointParCluster[k];

        for (int j = 0; j < k; ++j)
        {
            nbPointParCluster[j] = 0;
            for (int i = 0; i < clusters[0].m; ++i)
            {
                clusters[j].contenu[0][i] = 0;
            }

        }


        for (int i = 0; i < X.n; i++)
        {
            zmq_recv (receiver, &assign, sizeof(assign), 0);
            nbPointParCluster[assign[1]]++;
            for (int j = 0; j < clusters[assign[1]].m; j++)
            {
                clusters[assign[1]].contenu[0][j] += X.contenu[assign[0]][j];
            }
        }

        for (int j = 0; j < k; ++j)
        {
            for (int i = 0; i < clusters[j].m; ++i)
            {
                clusters[j].contenu[0][i] /= nbPointParCluster[j];
            }

        }

    

        // mise à jour des centroides : (clusters)


        error = calcul_erreur(old_clusters,clusters, k);

        for(int i =0; i<k; i++){
            freeMatrice(&old_clusters[i]);
        }
        free(old_clusters);

        for (int i = 0; i < k; ++i)
        {
            print(clusters[i]);
        }
        printf("Epoch : %d => %f\n\n\n\n",iter,error );

        iter++;
        s_sleep(1);

    }

 

    zmq_close (receiver);
    zmq_close (sender);
    zmq_close (publisher);
    zmq_ctx_destroy (context);
    return 0;
}