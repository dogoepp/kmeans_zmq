#ifndef _matrice
#define _matrice
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct {
   int n;
   int m;
   long double** contenu;
}matrice;

typedef struct {
   int n;
   int m;
   char*** contenu;
}matriceChar;

void init(matriceChar*,int ,int);
int countword(matriceChar,char*,int);
int countwordWhere(matriceChar,char*,int,matriceChar,char*);
matriceChar concatLineC(matriceChar,matriceChar);
matriceChar selectDistinct(matriceChar,int); // selectionne les elements distincts de la colonne en parametre
matriceChar extractLineC(matriceChar,int);
matriceChar vide(int,int);
int containt(matriceChar,char*,int);
int containtM(matrice A,long double  mot,int colone);
void printC(matriceChar);
matriceChar extractC(matriceChar A,int n1, int n2, int m1, int m2);
matrice selectDistinctM(matrice,int); // selectionne les elements distincts de la colonne en parametre



matrice inverse(matrice A); // (1/det(A))* transposé(comatrice)

matrice product(matrice A, matrice B);
void copy(matrice * copi,matrice A);
matrice sum(matrice A, matrice B);

matrice sous(matrice A, matrice B);
matrice comatrice(matrice A);
void extract(matrice * r, matrice A,int n1, int n2, int m1, int m2); // extrait une sous matrice de la ligne n1 à n2 et de la colonne m1 à m2
matrice sousMat(matrice A,int ligne); // pour le calcul du determinant, permet d'extraire la sous matrice ne contenant pas la ligne i et la colnne 1

matrice copySauf(matrice A,int ligne, int colonne);// permet de copier la matrice initiale sans la ligne i et sans la colonne j. pour l'inverse
matrice transpose(matrice A);

double determinant(matrice A);
void produit_scalaire_matrice(long double alpha,matrice *A);
// soustrait la deuxième matrice de la première, resultat dans la premiere
void sous_interne(matrice *A, matrice B);
matrice ones(int n,int m);
matrice init_value(int n,int m,long double value);
matrice two(int n, int m);
matrice zeros(int n,int m);
matrice system_lineaire(matrice A);

void concatLine(matrice * initiale, matrice A);
void extractLine(matrice * line, matrice A,int l);
void extractColumn(matrice * column, matrice A, int c);
void concatColoumn(matrice * initiale, matrice A);
matrice id(int n);// retourne la matrice identité de taille n
int compter_nb_ligne(char * filename);
void print(matrice A);
int random_(int,int); // min et max inclus
void deleteLine(matrice * A,int line);
void mixer_matrice(matrice * A);
void mixer_matrices(matrice * A,matrice * B);
void freeMatrice(matrice * A);
void readMatrice(matrice * A, char * filename);
void readDatas(matrice * A,matrice * B, char * filename ,int ncolumn);
int contient(matrice A,long double v);
void write_in_file(matrice A,char * filename,const char *  sep);
long double distance(matrice a, matrice b);
matrice random_matrice(int n,int m);
void som_interne(matrice *A, matrice B);
matrice normaliser_centrer(matrice X);
matrice moyenne(matrice X);

#endif

