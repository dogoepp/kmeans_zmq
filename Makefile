main_client :
	gcc -o client -Wall K_workers.c matrice.c -lczmq -lzmq -lm
main_serveur:
	gcc -o serveur -Wall Kmeans.c matrice.c -lczmq -lzmq -lm
run_serveur: 
	./serveur data_white.csv 0.05 100 4
run_client:
	./client

